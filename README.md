# Física 1

![Bellezas físicas](portada.jpg)

Curso de Verano 2024 \
Universidad de Buenos Aires

## Prácticas

| Nro |      Titulo      | Enunciado                                                                                          | Solución                                                                                                      |
|-----|------------------|----------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 0   | Repaso matemático | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica0.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/practica0.pdf)
| 1   | Cinemática | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica1.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/practica1.pdf)
| 2   | Dinámica | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica2.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/practica2.pdf)
| 3   | Rozamiento | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica3.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/practica3.pdf)
| 4   | Movimiento oscilatorio | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica4.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/practica4.pdf)
| 5   | Sistemas no inerciales | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica5.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/practica5.pdf)
| 6   | Relatividad | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica6.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/practica6.pdf)
| 7   | Impulso lineal | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica7.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/practica7.pdf)
| 8   | Impulso angular | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica8.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/Practica%208/practica8.pdf)
| 9   | Trabajo y energía | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica9.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/Practica%208/practica9.pdf)
| 10   | Teoremas de conservación | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica10.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/Practica%208/practica10.pdf)
| 11   | Gravitación | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica11.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/Practica%208/practica11.pdf)
| 12   | Cinemática del rígido | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica12.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/Practica%208/practica12.pdf)
| 13   | Dinámica del rígido | [Enunciado](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/enunciados/practica13.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-fisica-1/-/blob/main/practicas/soluciones/Practica%208/practica13.pdf)